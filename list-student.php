<!DOCTYPE html>
<html lang="en">
<?php
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
$numberStudent = 0;
$listNameStudent = array("1" => "Nguyễn Văn A", "2" => "Trần Thị B", "3" => "Nguyễn Hoàng C", "4" => "Đinh Quang D");
$listDepartmentStudent = array("1" => "MAT", "2" => "MAT", "3" => "KDL", "4" => "KDL");
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="list-student.css">
</head>

<body>
    <div class="list-student-content">
        <div class="table-1">
            <table id="table-content1">

                <form>
                    <tr>

                        <td>
                            <label>Khoa</label>
                        </td>
                        <td>
                            <select name="department" id="select-item" name="department">
                                <?php

                                foreach ($departmentArray as $key => $value) {
                                    echo ("
                                            <option value = \"$key\">$value</option>
                                        ");
                                }
                                ?>
                    </tr>
                    <tr>
                        <td><label>Từ khóa</label></td>
                        <td><input type='text' id='key' name="address"></td>
                    </tr>
                </form>
            </table>
        </div>
        <form id="btn-search">
            <input type='submit' class='btn-box' value="Tìm kiếm">
        </form>


        <div class="number-student">
            <?php
            $numberStudent = count($listNameStudent);
            echo "Số sinh viên tìm thấy: $numberStudent"
            ?>
            <form id="btn-add" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                
                <input type='submit' class='btn-box' value="Thêm" name="btn-add">
                <?php
                    if(isset($_POST["btn-add"])) {
                        header('location: register.php');
                    }
                ?>
            </form>

        </div>



        <div class="table-2">
            <table id="table-content2">
                <tr class="label-content">
                    <td class="no">No</td>
                    <td class="name-student">Tên sinh viên</td>
                    <td class="department">Khoa</td>
                    <td class="action">Action</td>
                </tr>
                <?php
                foreach ($listNameStudent as $key => $value) {
                    $keyDepartMent = $listDepartmentStudent[$key];
                    echo ("  
                        <tr>
                            <td class=\"no\">$key</td>
                                <td class=\"name-student\">$value</td>
                                <td class=\"department\">$departmentArray[$keyDepartMent]</td>
                                <td>
                                            
                                    <div class=\"btn-action-content\">
                                        <form>
                                            <input type=\"submit\" value=\"Xóa\" id=\"btn-action\">
                                            <input type=\"submit\" value=\"Sửa\" id=\"btn-action\">
                                        </form>
                                    </div>
                    
                            </td>
                                
                            
                            
                        </tr>
                    ");
                }
                ?>

            </table>
        </div>
    </div>

</body>

</html>